#!/bin/sh

PIC_DIR=/media/pi/foo/denlapse/images
GIF_NAME=timelapse.gif
GIF_PATH=$PIC_DIR/../$GIF_NAME
NERDSTER_REPO=/media/pi/foo/nerdster.org
POST_PATH=$NERDSTER_REPO/_posts/2016-6-12-denver-timelapse.md

echo "Started at "`date`

# Use the keys
eval `keychain --noask --eval id_rsa`
eval `keychain --noask --eval git`

# Make the gif
cd $PIC_DIR
TMP_GIF="tmpgif.gif"
convert -limit memory 64MiB -limit map 64MiB -delay 10 -resize 35% -loop 0 *.JPG $TMP_GIF && gifsicle --optimize=03 --colors=256 --loop $TMP_GIF > $GIF_PATH
rm $TMP_GIF

# Commit to the denlapse repo, already in the dir
git pull origin master
git add -A
git commit -m "Auto add gif to repo."
git push origin master

# Commit the new gif to the nerdster repo
cp $GIF_PATH $NERDSTER_REPO/images/denlapse/$GIF_NAME
cd $NERDSTER_REPO
git pull origin master
# Change the last update time
sed -i 's/Last updated: .*/Last updated: '"`date +"%r, %b %d, %Y"`"'/' $POST_PATH
git add images/denlapse/$GIF_NAME $POST_PATH
git commit -m "Auto new timelapse from Pi/iPhone team"
git push origin master

echo "Finished at "`date`
