#!/bin/sh

PIC_DIR=/media/pi/foo/denlapse/images

echo "Started at "`date`

# Use the keys
eval `keychain --noask --eval id_rsa`
eval `keychain --noask --eval git`

# Take the pic
ssh denlapse "activator send com.apple.camera"
sleep 5
ssh denlapse "activator send libactivator.camera.invoke-shutter"
sleep 3

# Grab the pic
for f in `ssh denlapse "find /private/var/mobile/Media/DCIM -name \"*.JPG\""`; do
	echo "Took pic $f, copying to storage"
	scp denlapse:$f $PIC_DIR
	echo "Removing remote $f"
	ssh denlapse "rm $f"
done

# Also clean up thumbnails
for f in `ssh denlapse "find /private/var/mobile/Media/PhotoData/Thumbnails -name \"*.JPG\""`; do
	echo "Removing remote $f"
	ssh denlapse "rm -rf $f"
done

# Commit to the denlapse repo
cd $PIC_DIR
git pull origin master
git add -A
git commit -m "Auto add pic to repo."
git push origin master

echo "Finished at "`date`
