#!/bin/sh
# Run from root of dir, like: ./bin/manual_mp4.sh

PIC_DIR=./images
TLAPSE_NAME=timelapse.mp4
NERDSTER_REPO=${HOME}/nerdster.org
POST_PATH=$NERDSTER_REPO/_posts/2016-6-12-denver-timelapse.md

echo "Started at "`date`

# Make the timelapse
cd $PIC_DIR
# Get the latest pics
git pull origin master
# "yes" to confirm overwrite
echo "Making the video..."
yes | ffmpeg -framerate 15 -pattern_type glob -i '*.JPG' -c:v libx264 -pix_fmt yuv420p ../$TLAPSE_NAME
cd -

# Update the timestamp
cd $NERDSTER_REPO
git pull origin master
# Change the last update time
sed -i 's/Last updated: .*/Last updated: '"`date +"%r, %b %d, %Y"`"'/' $POST_PATH
git add $POST_PATH
git commit -m "New timelapse timestamp"
git push origin master
cd -

# Add the new gif to the nerdster repo (has to happen after jekyll builds/replaces
# the site).
scp $TLAPSE_NAME nerdster.org:/usr/share/nginx/static/$TLAPSE_NAME

echo "Finished at "`date`
